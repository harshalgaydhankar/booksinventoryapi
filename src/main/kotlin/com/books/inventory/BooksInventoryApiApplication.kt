package com.books.inventory

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BooksInventoryApiApplication

fun main(args: Array<String>) {
	runApplication<BooksInventoryApiApplication>(*args)
}
